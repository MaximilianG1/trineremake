using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShitController : MonoBehaviour
{
    Rigidbody rb;
    [SerializeField] private float jumpForce;

    [SerializeField] private string rightButton;
    [SerializeField] private string leftButton;
    [SerializeField] private string jumpButton;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(rightButton))
        {
            rb.velocity += Vector3.right*0.1f;
        }

        if (Input.GetKey(leftButton))
        {
            rb.velocity += Vector3.left*0.1f;
        }

        if (Input.GetKeyDown(jumpButton))
        {
            rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
        }
    }
}
