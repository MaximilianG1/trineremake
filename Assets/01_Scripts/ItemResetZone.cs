using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemResetZone : MonoBehaviour
{
    private Collider colid;

    private void Awake() 
    {
        colid = GetComponent<Collider>();
    }

    private void OnTriggerEnter(Collider other) 
    {
        IResetable resetableItem = other.gameObject.GetComponent<IResetable>();

        if (resetableItem != null)
        {
            resetableItem.Reset();
        }
    }
}
