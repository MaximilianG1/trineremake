using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagicItemBehaviour : MonoBehaviour, IResetable
{
    private Rigidbody rb;
    [SerializeField] private GameObject gravityHalo;
    [SerializeField] private GameObject freezeHalo;

    private bool isFrozen;

    [SerializeField] LayerMask targetLayer;
    
    private Vector3 respawnPosition;
    

    [SerializeField] private float teleportOffset;

    private Renderer rendr;
    

    private void Awake() 
    {
        rb = GetComponent<Rigidbody>();
        rendr = GetComponent<Renderer>();
        
    }

    private void Start() 
    {
        SetRespawnPosition(this.transform.position);
    }

    public void OnFreezeSpell()
    {
        if(isFrozen)
        {
            rb.constraints = RigidbodyConstraints.FreezePositionZ;
            isFrozen = false;
            freezeHalo.SetActive(false);
        }
        else
        {
            rb.constraints = RigidbodyConstraints.FreezeAll;
            isFrozen = true;
            freezeHalo.SetActive(true);
        }
    }

    public void OnGravitySpell()
    {
        if(rb.useGravity)
        {
            rb.useGravity = false;
            gravityHalo.SetActive(true);
        }
        else
        {
            rb.useGravity = true;
            gravityHalo.SetActive(false);
        }
    }

    private void CancelAllSpell()
    {
        rb.velocity = Vector3.zero;
        rb.useGravity = true;
        gravityHalo.SetActive(false);
        rb.constraints = RigidbodyConstraints.FreezePositionZ;
        isFrozen = false;
        freezeHalo.SetActive(false);
    }



    public void Reset()
    {
        Vector3 spawnPosition = respawnPosition;
        while(IsRespawnPositionBlocked(spawnPosition))
        {
            spawnPosition.y += teleportOffset;
        }

        // reset tout
        transform.position = spawnPosition;
        this.CancelAllSpell();
        
    }

    public void SetRespawnPosition(Vector3 newPos)
    {
        respawnPosition = newPos;
    }

    public bool IsRespawnPositionBlocked(Vector3 spawnPosition)
    {
        Bounds bounds = rendr.bounds;
        return Physics.OverlapBox(spawnPosition, bounds.extents, Quaternion.identity, targetLayer).Length != 0;
    }



    // public void OnDrawGizmosSelected()
    // {
    //     var r = GetComponent<Renderer>();
    //     if (r == null)
    //         return;
    //     var bounds = r.bounds;
    //     Gizmos.matrix = Matrix4x4.identity;
    //     Gizmos.color = Color.blue;
    //     Gizmos.DrawWireCube(bounds.center, bounds.extents * 2);
    // }

     



}
