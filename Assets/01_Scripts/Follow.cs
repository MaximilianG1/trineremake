using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follow : MonoBehaviour
{
    [SerializeField] private Transform target;

    private Camera targetCam;
    private Camera myCam;

    private void Awake()
    {
        targetCam = target.GetComponent<Camera>();
        myCam = GetComponent<Camera>();
    }

    private void Update()
    {
        transform.position = target.position;
        myCam.fieldOfView = targetCam.fieldOfView;
    }
}
