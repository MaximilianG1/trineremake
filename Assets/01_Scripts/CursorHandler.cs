using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.EventSystems;
using System.Runtime.InteropServices.WindowsRuntime;

public class CursorHandler : MonoBehaviour
{
    public static Vector2 aimDirection;
    [SerializeField] private PlayerController playerController;

    [SerializeField] private float returnTime = 0.8f;

    public void OnAim(InputAction.CallbackContext value)
    {
        aimDirection = value.ReadValue<Vector2>();

        if (aimDirection.x == 0f && aimDirection.y == 0f)
        {
            return;
        }
        else
        {
            if (playerController.FacingRight)
            {
                transform.localEulerAngles = new Vector3(0f, 0f, Mathf.Atan2(aimDirection.x, aimDirection.y) * -180 / Mathf.PI + 90f);
                playerController.AimDirection = transform.right;
            }
            else
            {
                transform.localEulerAngles = new Vector3(0f, 0f, Mathf.Atan2(aimDirection.x, aimDirection.y) * 180 / Mathf.PI + 90f);
                playerController.AimDirection = -transform.right;
            }
        }
    }
}
