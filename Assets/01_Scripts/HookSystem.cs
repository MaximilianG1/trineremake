using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class HookSystem : MonoBehaviour
{
    public PlayerController playerControlerRef;

    [SerializeField] private GameObject hand;
    [SerializeField] GameObject shootableObjectPrefab;
    GameObject shootableObject;
    HookObject shootableHook;
    [SerializeField] float shootableProjectionDistance;
    bool isShooting;
    public bool isSwinging;
    public bool isPulling;

    public void ShootObject(InputAction.CallbackContext value) //shoot l'objet
    {
        if (value.started)
        {
            if (isShooting)
            {
                isShooting= false;
                DisableObject();
            }
            else
            {
                Vector3 direction = playerControlerRef.AimDirection;
                if (shootableObject != null)
                {
                    shootableObject.SetActive(true);
                    //shootableObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
                    shootableObject.transform.position = hand.transform.position;
                }
                else
                {
                    shootableObject = Instantiate(shootableObjectPrefab, hand.transform.position, new Quaternion(0, 0, 0, 0));
                    shootableHook = shootableObject.GetComponent<HookObject>();
                    shootableHook.playerRB = GetComponent<Rigidbody>();
                    shootableHook.SetHookSystem(this);
                }
                shootableObject.GetComponent<Rigidbody>().AddForce(direction * shootableProjectionDistance, ForceMode.Impulse);
                isShooting= true;
            }
        }
    }

    public void DisableObject() // d�truit l'objet
    {
        shootableHook.UnableSwingObject();
        shootableObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
        shootableObject.GetComponent<Rigidbody>().isKinematic = false;
        shootableObject.SetActive(false);
    }

    public void Hook(InputAction.CallbackContext value)
    {
        if (value.started)
        {
            isSwinging = false;
            shootableHook.Hook();          
        }
    }

    public void OnPull(InputAction.CallbackContext value)
    {
        Vector2 pullMovement = value.ReadValue<Vector2>();

        if (value.performed)
        {
            if (isSwinging && shootableHook)
            {
                if (pullMovement.y > 0)
                {
                    isPulling = true;
                    Debug.Log("pull");
                    shootableHook.Pull();
                }
                else if (pullMovement.y < 0)
                {
                    isPulling = true;
                    Debug.Log("reverse pull");
                    shootableHook.ReversePull();
                }
            }
        }

        if (value.canceled)
        {
            if (shootableHook)
            {
                isPulling = false;
                shootableHook.stopFreezRotatioNZ();
            }
        }
    }

    public void OnSwing(InputAction.CallbackContext value)
    {
        if (value.performed)
        {
            if (isSwinging && !isPulling)
            {
                if (playerControlerRef.MoveX > 0)
                {
                    Debug.Log("Swing Right");
                    shootableHook.SwingRight();
                }
                else if (playerControlerRef.MoveX < 0)
                {
                    Debug.Log("Swing Left");
                    shootableHook.SwingLeft();
                }
            }
        }
    }
}