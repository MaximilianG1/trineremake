using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HookObject : MonoBehaviour
{

    [SerializeField] LayerMask maskInteract; //le masque avec lequel int�ragit le hook
    [SerializeField] Rigidbody physicsHook;

     HookSystem systemHook;

     Rigidbody objectTouchRef;
     public Rigidbody playerRB;

    HingeJoint joint;

    [SerializeField] float speedVelo;
    [SerializeField] float speedHookAttire;
    [SerializeField] float speedSwing;

    public bool isHooking = false;
    bool whereShootableOff;

    private void OnTriggerEnter(Collider other) // quand le projectile s'approche de la cible
    {
        if (other.gameObject.CompareTag("Hookable"))
        {
            Vector3 toGo = other.gameObject.transform.position;
            Vector3 direction = new Vector3(toGo.x - transform.position.x, toGo.y - transform.position.y, toGo.z - transform.position.z).normalized;

            physicsHook.velocity = new Vector3(direction.x * speedVelo, direction.y * speedVelo, direction.z * speedVelo);
        }
    }

    public void SetHookSystem(HookSystem hook) // set la r�f�rence du hook system
    {
        systemHook = hook;
    }

    private void OnCollisionEnter(Collision collision) //quand il touche la cible 
    {
        if (collision.gameObject.CompareTag("Hookable"))
        {
            objectTouchRef = collision.gameObject.GetComponent<Rigidbody>() ;
            systemHook.isSwinging= true ;
            Debug.Log("�a connecte");
                InteractWithObject();
            
        }
        else DestroyProjectil();
    }

    private void InteractWithObject() // quand il int�ragit avec
    {
        physicsHook.isKinematic = true;
        EnableSwingObject();
    }

    private void EnableSwingObject() // active le swing
    {
        playerRB.constraints = RigidbodyConstraints.None;
        //playerRB.constraints = RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationX;
        playerRB.velocity= Vector3.zero;
        joint = gameObject.AddComponent<HingeJoint>();
        joint.connectedBody = playerRB;
        joint.axis = new Vector3(0, 0, 1);
        joint.anchor = new Vector3(0, 0.5f, 0);
        joint.autoConfigureConnectedAnchor = false;
        joint.connectedAnchor = new Vector3(transform.position.x - playerRB.position.x, transform.position.y - playerRB.position.y, transform.position.z - playerRB.position.z);
    }

    public void UnableSwingObject() // desactive le swing
    {
        if (joint)
        {
            systemHook.isSwinging= false ;
            playerRB.constraints = RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotation;
            playerRB.gameObject.transform.rotation = Quaternion.identity;
            Destroy(joint);
        }
        else
        {
            playerRB.constraints = RigidbodyConstraints.FreezeRotation;
        }
    }

    public void Hook()
    {
        if (!isHooking)
        {
            if (!(objectTouchRef.constraints == RigidbodyConstraints.FreezeAll) )
            {
                whereShootableOff = true;

                UnableSwingObject();

                Vector3 toGo = objectTouchRef.gameObject.transform.position;
                Vector3 direction = new Vector3(transform.position.x - toGo.x, transform.position.y - toGo.y, transform.position.z - toGo.z).normalized;
                objectTouchRef.velocity = new Vector3(direction.x * speedHookAttire, direction.y * speedHookAttire, direction.z * speedHookAttire);
                isHooking = true;
            } 
        }
        else
        {
            if (whereShootableOff)
            {
                EnableSwingObject();
                objectTouchRef.velocity = Vector3.zero;
                isHooking = false;
                whereShootableOff = false;
            }
        }
    }

    public void stopFreezRotatioNZ()
    {
        playerRB.constraints = RigidbodyConstraints.None;
    }

    public void Pull()
    {
        playerRB.constraints = RigidbodyConstraints.FreezeRotationZ;
        joint.connectedAnchor = new Vector3(joint.connectedAnchor.x, joint.connectedAnchor.y - (speedHookAttire), joint.connectedAnchor.z);
    }
    public void ReversePull()
    {
        playerRB.constraints = RigidbodyConstraints.FreezeRotationZ;
        joint.connectedAnchor = new Vector3(joint.connectedAnchor.x, joint.connectedAnchor.y + (speedHookAttire), joint.connectedAnchor.z);
    }

    public void SwingRight()
    {
        //add velocity
        playerRB.AddForce(new Vector3(speedSwing,0 , 0));
    }

    public void SwingLeft()
    {
        //add velocity
        playerRB.AddForce(new Vector3(-speedSwing, 0, 0));
    }

    private void DestroyProjectil() // detruit le projectile
    {
        physicsHook.isKinematic = false;
        UnableSwingObject();
        systemHook.DisableObject();
    }
}
