using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class GravityController : MonoBehaviour
{
    [SerializeField] private float radius;
    [SerializeField] private GameObject sphereDisplay;
    [SerializeField] LayerMask targetLayer;

    [SerializeField] private string gravityButton;
    [SerializeField] private string freezeButton;

    private List<Rigidbody> rbList = new List<Rigidbody>();


    // Start is called before the first frame update
    void Start()
    {
        sphereDisplay.transform.localScale = new Vector3(1*(radius*2), 1*(radius*2), 0.5f);
    }

    private void Update() 
    {
        if (Input.GetKeyDown(freezeButton))
        {
            FreezeOrReleaseAllItems();
        }
        if (Input.GetKeyDown(gravityButton))
        {
            EnableItemsGravity();
        }
    }

    private void FreezeOrReleaseAllItems()
    {
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, radius, targetLayer);//0000001000);
        foreach (Collider hit in hitColliders)
        {
            MagicItemBehaviour magicItem = hit.GetComponent<MagicItemBehaviour>();
            magicItem.OnFreezeSpell();
        }
    }

    private void EnableItemsGravity()
    {
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, radius, targetLayer);//0000001000);
        foreach (Collider hit in hitColliders)
        {
            MagicItemBehaviour magicItem = hit.GetComponent<MagicItemBehaviour>();
            magicItem.OnGravitySpell();
        }
    }

    public void OnToggleGravity(InputAction.CallbackContext value)
    {
        if (value.started)
        {
            EnableItemsGravity();
        }
    }

    public void OnToggleFreeze(InputAction.CallbackContext value)
    {
        if (value.started)
        {
            FreezeOrReleaseAllItems();
        }
    }

    
}
