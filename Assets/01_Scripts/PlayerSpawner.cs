using UnityEngine;
using UnityEngine.InputSystem;
using Cinemachine;

public class PlayerSpawner : MonoBehaviour
{
    enum Choice { BOTH, P1ONLY, P2ONLY }

    [SerializeField] private GameObject player1Prefab;
    [SerializeField] private GameObject player2Prefab;
    [SerializeField] private Choice choice = Choice.BOTH;

    [SerializeField] private Transform player1Spawn;
    [SerializeField] private Transform player2Spawn;

    static PlayerInput P1, P2;

    private bool hasSpawn = false;
    [SerializeField] private CinemachineTargetGroup targetGroup;

    private void Awake()
    {
        SpawnBothPlayers();

        Invoke("DestroyPlayer", 1f);
    }

    private void DestroyPlayer()
    {
        if (hasSpawn)
        {
            switch (choice)
            {
                case Choice.BOTH:                   
                    break;
                case Choice.P1ONLY:
                    Destroy(P2.gameObject);
                    break;
                case Choice.P2ONLY:
                    Destroy(P1.gameObject);
                    break;
                default:
                    break;
            }
        }
    }

    private void SpawnBothPlayers()
    {
        P1 = SpawnPlayer(1, player1Prefab);
        P2 = SpawnPlayer(2, player2Prefab);

        P1.transform.position = player1Spawn.position;
        P2.transform.position = player2Spawn.position;

        
        hasSpawn = true;

        if (targetGroup)
        {
            targetGroup.AddMember(P1.transform, 1f, 0);
            targetGroup.AddMember(P2.transform, 1f, 0);
        }
        else
        {
            Debug.Log("Attention, aucun Target group de set");
        }
    }

    private PlayerInput SpawnPlayer(int id, GameObject prefab)
    {
        PlayerInput newPlayer = null;

        if (Keyboard.current != null)
        {
            newPlayer = PlayerInput.Instantiate(prefab, controlScheme: "P" + id, pairWithDevice: Keyboard.current);
            newPlayer.gameObject.name = "Player " + id;
        }
        else if (Gamepad.current != null)
        {
            newPlayer = PlayerInput.Instantiate(prefab, controlScheme: "P" + id, pairWithDevice: Gamepad.current);
            newPlayer.gameObject.name = "Player " + id;
        }

        return newPlayer;
    }
}
