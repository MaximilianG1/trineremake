using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IResetable
{
    void SetRespawnPosition(Vector3 newPos); // A set au start, ou aux checkpoints pour les players
    void Reset(); 
    bool IsRespawnPositionBlocked(Vector3 spawnPosition);
    

}
