using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class GrabSystem : MonoBehaviour
{
    public PlayerController playerControlerRef;

    [SerializeField] private GameObject hand;
    [SerializeField] GameObject shootableObjectPrefab;
    GameObject shootableObject;
    GrabObject shootableGrab;
    [SerializeField] float shootableProjectionDistance;
    bool isShooting;
    public bool isGrabing;

    public void ShootObject(InputAction.CallbackContext value) //shoot l'objet
    {
        if (value.started)
        {
            if (isShooting)
            {
                isShooting = false;
                DisableObject();
            }
            else
            {
                Vector3 direction = playerControlerRef.AimDirection;
                if (shootableObject != null)
                {
                    shootableObject.SetActive(true);
                    //shootableObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
                    shootableObject.transform.position = hand.transform.position;
                }
                else
                {
                    shootableObject = Instantiate(shootableObjectPrefab, hand.transform.position, new Quaternion(0, 0, 0, 0));
                    shootableGrab = shootableObject.GetComponent<GrabObject>();
                    shootableGrab.playerRB = GetComponent<Rigidbody>();
                    shootableGrab.SetHookSystem(this);
                }
                shootableObject.GetComponent<Rigidbody>().AddForce(direction * shootableProjectionDistance, ForceMode.Impulse);
                isShooting = true;
            }
        }
    }

    public void DisableObject() // d�truit l'objet
    {
        shootableObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
        shootableObject.GetComponent<Rigidbody>().isKinematic = false;
        shootableObject.SetActive(false);
    }

    public void OnGrab(InputAction.CallbackContext value)
    {
        Vector2 pullMovement = value.ReadValue<Vector2>();

        if (value.performed)
        {
            if (isGrabing && shootableGrab)
            {
                if (pullMovement.y < 0)
                {
                    Debug.Log("d�but grab");
                    shootableGrab.Hook();
                }
            }
        }

        if (value.canceled)
        {
            if (isGrabing && shootableGrab)
            {
                Debug.Log("Fin grab");
                shootableGrab.StopHook();
            }
        }
    }
}
