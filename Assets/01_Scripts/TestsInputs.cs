using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class TestsInputs : MonoBehaviour
{
    public Vector2 movement;

    public bool isSwinging = false;

    public void SwingToward(InputAction.CallbackContext value)
    {
        movement = value.ReadValue<Vector2>();

        if (isSwinging)
        {
            if (movement.x < 0)
            {
                Debug.Log("Swing vers la gauche");
            }
            else if (movement.x > 0)
            {
                Debug.Log("Swing vers la droite");
            }
        }
    }

    public void SimulateHook(InputAction.CallbackContext value)
    {
        if (value.started)
        {
            isSwinging = true;
            Debug.Log("started : isSwinging = true");
        }

        if (value.canceled)
        {
            isSwinging = false;
            Debug.Log("canceled : isSwinging = false");
        }
    }


}
