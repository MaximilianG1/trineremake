using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.EventSystems;

public class PlayerController : MonoBehaviour
{
    [Header("Movement")]
    [SerializeField] private float moveSpeed;
    [SerializeField] private float jumpForce;
    [SerializeField] private bool isMoving = false;
    private Vector2 movement;
    private float moveX;
    public float MoveX
    {
        get { return moveX; }
    }

    private float moveY;
    public float MoveY
    {
        get { return moveY; }
    }

    [Header("Jump")]
    private bool tryToJump = false;

    [Header("Ref pour Voleur")]
    public HookSystem hookSystem;

    private Vector3 aimDirection;
    public Vector3 AimDirection
    {
        get { return aimDirection; }
        set { aimDirection = value; }
    }

    private bool grounded;
    private Animator animator;
    private Rigidbody rb;
    private bool facingRight;
    private float distToGround;

    public bool FacingRight
    {
        get { return facingRight; }
    }

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        animator = GetComponentInChildren<Animator>();
        facingRight = true;
    }

    private void Start()
    {
        distToGround = GetComponent<Collider>().bounds.extents.y;
    }

    private void Update()
    {     
        if (moveX > 0 && !facingRight)
        {
            Flip();
        }
        else if (moveX < 0 && facingRight)
        {
            Flip();
        }

        if (rb.velocity != Vector3.zero)
        {
            animator.SetFloat("Blend", 6f);
        }
        else
        {
            animator.SetFloat("Blend", 0f);
        }

        grounded = IsGrounded();
        animator.SetBool("isGrounded", grounded);
        
    }
    private void FixedUpdate()
    {
        if (tryToJump && IsGrounded())
        {
            Jump();
        }
        else if (tryToJump && !IsGrounded())
        {
            tryToJump = false;
        }

        if (hookSystem && !hookSystem.isSwinging || !hookSystem)
            rb.velocity = new Vector3(moveX * moveSpeed, rb.velocity.y, 0f);

    }

    public void OnMove(InputAction.CallbackContext value)
    {
        // calculate movement direction
        movement = value.ReadValue<Vector2>();
        moveX = movement.x;
        moveY = movement.y;
    }

    public void OnJump(InputAction.CallbackContext value)
    {
        if (value.started)
        {
            tryToJump = true;
        }
    }

    private void Jump()
    {
        rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
        animator.SetTrigger("Jump");
        tryToJump = false;
    }

    private bool IsGrounded()
    {
        bool isGrounded = Physics.Raycast(transform.position, Vector3.down, distToGround + 0.1f);

        return isGrounded;
    }

    private void Flip()
    {
        facingRight = !facingRight;

        Vector3 myScale = transform.localScale;
        myScale.x *= -1;
        transform.localScale = myScale;
    }
}
